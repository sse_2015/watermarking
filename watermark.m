%----------------------------------------------------------
% Parameters initialization
%----------------------------------------------------------
imagefiles = dir('resized_images/*.tif'); 
nfiles = length(imagefiles);    % Number of files found

sequences = fopen('sequences.txt','r');
S = fscanf(sequences, '%d %d', [2 Inf]);
fclose(sequences);

%----------------------------------------------------------
% Watermarking process
%----------------------------------------------------------
result = fopen('result.txt','w');
fprintf(result,'%6s %12s %12s %8s %10s %12s %12s %12s %10s %12s %16s\r\n', ...
    'seq','host', 'watermark', 'psnr', 'ncc_salt', 'ncc_gaussian', ...
    'ncc_poisson', 'ncc_speckle', 'ncc_mean', 'ncc_median', 'ncc_compression');

for i = 1:length(S)
    for j = 1:nfiles
        for k = 1:nfiles
            if j ~= k
				try
		            I = imread(strcat('resized_images/', imagefiles(j).name));
		            W = imread(strcat('resized_images/', imagefiles(k).name));
		            W = im2bw(W, graythresh(W));
		            [m_I, n_I] = size(I);
		            [m_W, n_W] = size(W);
		            scatter = zeros(1, m_W*n_W);
		            for m = 1:m_W*n_W
		                scatter(m) = S(1, i) * m + S(2, i);
		            end
		            W_ed = watermark_embedding(I, W, scatter);
		            
		            % Noise Attack
		            W_ed_salt = imnoise(W_ed, 'salt & pepper');
		            W_ex_1 = watermark_extraction(W_ed_salt, scatter, m_W, n_W);
		            
		            W_ed_gauss = imnoise(W_ed, 'gaussian');
		            W_ex_2 = watermark_extraction(W_ed_gauss, scatter, m_W, n_W);
		            
		            W_ed_poisson = imnoise(W_ed, 'poisson');
		            W_ex_3 = watermark_extraction(W_ed_poisson, scatter, m_W, n_W);
		            
		            W_ed_speckle = imnoise(W_ed, 'speckle');
		            W_ex_4 = watermark_extraction(W_ed_speckle, scatter, m_W, n_W);
		            
		            % Mean Attack
		            h = fspecial('average', [5, 5]);
		            meanImageAttacked = filter2(h, W_ed);
		            W_ex_5 = watermark_extraction(uint8(meanImageAttacked), scatter, m_W, n_W);
		            
		            % Median Attack
		            medianImageAttacked = medfilt2(W_ed);
		            W_ex_6 = watermark_extraction(uint8(medianImageAttacked), scatter, m_W, n_W);
		            
		            % Compression Attack
		        	compressionImageAttacked = compression(W_ed, 80);
		        	W_ex_7 = watermark_extraction(uint8(compressionImageAttacked), scatter, m_W, n_W);
		            
		            fprintf(result,'%6s %12s %12s %8.4f %10.4f %12.4f %12.4f %12.4f %10.4f %12.4f %16.4f\r\n', ...
		                strcat(num2str(S(1,i)), 'n+', num2str(S(2,i))), ...
		                imagefiles(j).name, imagefiles(k).name, ...
		                psnr(I, W_ed), ncc(W, W_ex_1), ncc(W, W_ex_2), ...
		                ncc(W, W_ex_3), ncc(W, W_ex_4), ncc(W, W_ex_5), ...
		                ncc(W, W_ex_6), ncc(W, W_ex_7));
				catch ME
					
				end
            end
        end
    end
end

fclose(result);
