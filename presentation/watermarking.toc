\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Proposed Method}{20}{0}{2}
\beamer@subsectionintoc {2}{1}{Watermark embedding}{20}{0}{2}
\beamer@subsectionintoc {2}{2}{Watermark extraction}{39}{0}{2}
\beamer@sectionintoc {3}{Experimental results}{41}{0}{3}
