function [ output ] = ncc( I, K )
%NCC(I,K) calculates normalized cross correlation ratio between I, K
%
%  Parameters:
%
%    Input, uint8 I(M,N), a grayscale image.
%
%    Input, uint8 K(M,N), a grayscale image.
%
%    Output, double OUT, the normalized cross correlation ratio between I, K.
%

if size(I) ~= size(K)
    warning('The size of input images need to be equal');
    return;
end

%
% Get the size of IM.
%
[m, n] = size(I);

X = double(I);
Y = double(K);

sum1 = 0.0;
sum2 = 0.0;
for i = 1:m
    for j = 1:n
        sum1 = sum1 + X(i, j) * Y(i, j);
        sum2 = sum2 + X(i, j) * X(i, j);
    end
end
output = sum1/sum2;

end

