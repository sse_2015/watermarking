function [ out ] = watermark_embedding( host, watermark, scatter )
%WATERMAKR_EMBEDDING  Watermark Embedding algorithm

[m, n] = size(host);
[a, b] = size(watermark);

bit1 = bitget(host, 1);
bit2 = bitget(host, 2);
bit3 = bitget(host, 3);
bit4 = bitget(host, 4);
bit5 = bitget(host, 5);
bit6 = bitget(host, 6);
bit7 = bitget(host, 7);
bit8 = bitget(host, 8);

r1 = reshape(bit1, 1, []);
r2 = reshape(bit2, 1, []);
r3 = reshape(bit3, 1, []);
r4 = reshape(bit4, 1, []);
r5 = reshape(bit5, 1, []);
r6 = reshape(bit6, 1, []);
r7 = reshape(bit7, 1, []);
r8 = reshape(bit8, 1, []);

w = watermark; %reshape(bitget(watermark, 8), 1, []);

v = [r1 r2 r3 r4 r5 r6 r7 r8];
for i = 1:(a*b)
    v(scatter(i)) = w(i);
end

v1 = v(1:(m*n));
v2 = v((m*n+1):(2*m*n));
v3 = v((2*m*n+1):(3*m*n));
v4 = v((3*m*n+1):(4*m*n));
v5 = v((4*m*n+1):(5*m*n));
v6 = v((5*m*n+1):(6*m*n));
v7 = v((6*m*n+1):(7*m*n));
v8 = v((7*m*n+1):(8*m*n));

b1 = reshape(v1, m, n);
b2 = reshape(v2, m, n);
b3 = reshape(v3, m, n);
b4 = reshape(v4, m, n);
b5 = reshape(v5, m, n);
b6 = reshape(v6, m, n);
b7 = reshape(v7, m, n);
b8 = reshape(v8, m, n);

out=b1+b2*2+b3*4+b4*8+b5*16+b6*32+b7*64+b8*128;

end

