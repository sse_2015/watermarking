function [ watermark ] = watermark_extraction( watermarked, scatter, a, b )
%WATERMARK_EXTRACTION Watermark extraction algorithm

bit1 = bitget(watermarked, 1);
bit2 = bitget(watermarked, 2);
bit3 = bitget(watermarked, 3);
bit4 = bitget(watermarked, 4);
bit5 = bitget(watermarked, 5);
bit6 = bitget(watermarked, 6);
bit7 = bitget(watermarked, 7);
bit8 = bitget(watermarked, 8);

r1 = reshape(bit1, 1, []);
r2 = reshape(bit2, 1, []);
r3 = reshape(bit3, 1, []);
r4 = reshape(bit4, 1, []);
r5 = reshape(bit5, 1, []);
r6 = reshape(bit6, 1, []);
r7 = reshape(bit7, 1, []);
r8 = reshape(bit8, 1, []);

w = zeros(1, a*b);

v = [r1 r2 r3 r4 r5 r6 r7 r8];
for i = 1:(a*b)
    w(i) = v(scatter(i));
end

watermark = reshape(w, a, b);
end

