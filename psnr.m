function out = psnr(I, K)
%PSNR(I,K) calculates peak signal-to-noise ratio between I, K
%
%  Parameters:
%
%    Input, uint8 I(M,N), a grayscale image.
%
%    Input, uint8 K(M,N), a grayscale image.
%
%    Output, double OUT, the peak signal-to-noise ratio between I, K.
%
 
%
% Validate parameters
%
if size(I) ~= size(K)
    warning('The size of input images need to be equal');
    return;
end

%
% Get the size of IM.
%
[m, n] = size(I);
 
%
% Convert I, K to double.
%
X = double(I);
Y = double(K);
 
%
% Calculate MSE
%
mse = double(0);
for i=1:m
    for j=1:n
        mse = mse + ((X(i,j) - Y(i,j))^2);
    end
end
mse = mse/(m * n);
 
%
% Calculate PNRS
%
out = 20*log10(255) - 10*log10(mse);
end
